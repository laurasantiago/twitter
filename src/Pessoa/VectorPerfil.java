package Pessoa;

import java.util.Scanner;
import java.util.Vector;

import Erros.UJCException;
import Erros.UNCException;
import Repos.IRepositorioUsuario;

public class VectorPerfil implements IRepositorioUsuario {
	Vector<Perfil> vetor = new Vector<Perfil>();

	public void cadastrarUsuario(Perfil usuario) throws UJCException {
		try {
			Perfil a = buscar(usuario.getUsuario());
//			if(a==null) {
//				System.out.println("s");
//			} td ok
			if(a!=null) {
				//System.out.println(a.getUsuario());
				throw new UJCException(usuario.getUsuario()); //se o usuario ja existr
			}else { //se a for nulo, o user n existe ainda
				vetor.add(usuario);
				System.out.println("o usuario @"+ usuario.getUsuario() +" foi cadastrado com sucesso");
			}
		} catch (UJCException e) {
			e.printStackTrace();
		}
			
	}

	public Perfil buscar(String usuario) {
		int i = 0;
		System.out.println("verificando se o usuario @" + usuario +" existe...");
		//System.out.println("tamanho do vetor:" + vetor.size());
		boolean a = false;
		try {
			if(vetor.size()!=0) {
				while(a==false) {
					a = vetor.get(i).getUsuario().equals(usuario);
					i = i + 1;

				}
				System.out.println("usuario achado! de nome @"+ vetor.get(i-1).getUsuario());
				return vetor.get(i-1);
			}else {
				return null;
			}
			
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			//System.out.println("o usuario nao existe");
			return null;
		}
		
		
	}

	public void atualizar(Perfil usuario) {
		
		Scanner ler = new Scanner(System.in);
		int i=0;
		int j = 0;
		String nome;
		while(i!=3) {
			System.out.println("insira 1 para mudar seu @, 2 para mudar se esta ativo e 3 para sair");
			i = ler.nextInt();
			if(i==1) {
				System.out.println("insira qual nome deseja");
				nome = ler.next();
				Perfil ver = buscar(nome);
				Vector<Tweet> tl = usuario.getTimeline();
				Vector<Tweet> tts = usuario.getMytweets();
				if(ver==null) {
					usuario.setUsuario(nome);
					while(j<usuario.getMytweets().size()) {
						usuario.getMytweets().get(j).setUsuario(nome); //setando o usuario dos tweets pro nome novo
						j = j + 1;
					}
					System.out.println("atualizado com sucesso\nnovo nome:"+usuario.getUsuario());
				}else {
					System.out.println("nao foi possivel atualizar");
				}
			} //fim do if i==1
			if(i==2) {
				if(usuario.isAtivo()==true) {
					usuario.setAtivo(false);
				}else {
					usuario.setAtivo(true);
				}
			}
		} //fim do while
	} //fim da funcao
	
	public void descadastrarUsuario(Perfil usuario) {
		vetor.remove(usuario);
	}
	
}
