package Pessoa;

import java.util.Vector;

public abstract class Perfil {
	private String usuario; //quarda o @batatafrita7
	private Vector<Perfil> seguidos; //guarda nomes
	private Vector<Perfil> seguidores;
	private Vector<Tweet> timeline;
	private Vector<Tweet> mytweets;
	private boolean ativo;
	
	public Perfil(String usuario) {
		this.usuario = usuario;
		this.ativo = true;
		Vector<Perfil> seguidos = new Vector<Perfil>();
		Vector<Perfil> seguidores = new Vector<Perfil>();
		Vector<Tweet> timeline = new Vector<Tweet>();
		Vector<Tweet> mytweets = new Vector<Tweet>();
		this.seguidos = seguidos;
		this.seguidores = seguidores;
		this.timeline = timeline;
		this.mytweets = mytweets;
	}
	public void addSeguido(Perfil usuario) {
		seguidos.add(usuario);
	}
	public void addSeguidor(Perfil usuario) {
		seguidores.add(usuario);
	}
	public void addTweet(Tweet tweet) {
		timeline.add(tweet);
		System.out.println("o tweet foi adicionado para a timeline de usuario @" + tweet.getUsuario());
	}
	public void addMyTweet(Tweet tweet) {
		mytweets.add(tweet);
		System.out.println("o tweet foi adicionado para o usuario @" + tweet.getUsuario());
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
		System.out.println("o tweet foi setado para o usuario @" + usuario);
	}
	public String getUsuario() {
		return usuario;
	}
	public Vector<Perfil> getSeguidos(){
		return seguidos;
	}
	public Vector<Perfil> getSeguidores(){
		return seguidores;
	}
	public Vector<Tweet> getTimeline(){
		return timeline;
	}
	public Vector<Tweet> getMytweets(){
		return mytweets;
	}

	public void setAtivo(boolean valor) {
		ativo = valor;
		System.out.println("o valor foi mudado com sucesso");
	}
	public boolean isAtivo() {
		return ativo;
	}
}
