package Pessoa;

import java.util.Scanner;
import java.util.Vector;

import Erros.MFPException;
import Erros.PDException;
import Erros.PEException;
import Erros.PIException;
import Erros.SIException;
import Erros.UJCException;
import Repos.IRepositorioUsuario;
import Repos.ITwitter;

public class MyTwitter implements ITwitter {
	private IRepositorioUsuario users;
	public MyTwitter(IRepositorioUsuario users) { //construtor com repo
		this.setUsers(users);
	}
	
	public void criarPerfil(Perfil usuario) throws PEException, UJCException {
		try {
			Perfil a = getUsers().buscar(usuario.getUsuario());
//			if(a==null) {
//				System.out.println("s");
//			} td ok
			if(a!=null) {
				//System.out.println(a.getUsuario());
				throw new PEException();
			}else { //se a for nulo, o user n existe ainda
				getUsers().cadastrarUsuario(usuario);
				
			}

		} catch (PEException e) {
			e.printStackTrace();
		}
	}

	public void cancelarPerfil(Perfil usuario) throws PIException, PDException {
		System.out.println("cancelando..");
		int i =0;
		try {
			//Perfil a = users.buscar(usuario.getUsuario());
			if(usuario==null) { //usuario n existe
				throw new PIException(); //OK
			}else { //se a for nulo, o user existe
				if(usuario.isAtivo()==false) {
					System.out.println("ss");
					System.out.println("inativ");
					throw new PDException();
				}else {
					i = 0;
					while(i<usuario.getSeguidores().size()) {
						usuario.getSeguidores().get(i).getTimeline().removeAll(tweets(usuario.getUsuario())); //removendo os tweets do usuario que vai ser cancelado da timeline de seus seguidores
						i = i+1;
					}
					getUsers().descadastrarUsuario(usuario);
				}
			}
		} catch (PIException e) {
			e.printStackTrace();
		}catch (PDException e2) {
			e2.printStackTrace();
		}
		
	}

	public void tweetar(String usuario, String mensagem) throws PIException, MFPException {
		try {
			Perfil perfil = getUsers().buscar(usuario);
			int i = 0;
			Perfil seg;
			if(perfil==null) {
				throw new PIException();
			}else {
				if(mensagem.length()>140 || mensagem.length()<1) {
					//ex de teste: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
					throw new MFPException();
				}else {
					//se estiver td ok
					Tweet tweet = new Tweet(); //cliando objeto tweet
					tweet.setUsuario(usuario); //definindo seu usuario ((DA CLASSE TWEET, N PERFIL)
					tweet.setMensagem(mensagem); //definindo sua mensagem
					perfil.addTweet(tweet); //adicionando na timeline do perfil
					perfil.addMyTweet(tweet); //adicionando nos tweets so do perfil
					i = 0;
					while(i<perfil.getSeguidores().size()) {
						seg = perfil.getSeguidores().get(i); //pega os seguidores
						seg.getTimeline().add(tweet); //adicionando o tweet do brother na timeline dos seguidores dele
						i = i + 1;
					}
				}
			}
		} catch (PIException e) {
			e.printStackTrace();
		}catch(MFPException e2) {
			e2.printStackTrace();
		}
		
		
	}

	public Vector<Tweet> tweets(String usuario) throws PIException, PDException {
		Perfil perfil = getUsers().buscar(usuario);
		try {
			if(perfil==null) {
				throw new PIException();
			}else {
				if(perfil.isAtivo()==false) {
					throw new PDException();
				}else {
					Vector<Tweet> tweets = perfil.getMytweets();
					return tweets;
				}
			}
		} catch (PIException e) {
			e.printStackTrace();
			return null;
		}catch(PDException e2) {
			e2.printStackTrace();
			return null;
		}
		
	}

	public void seguir(String seguidor, String seguido) throws PIException, PDException, SIException {
		Perfil p1 = getUsers().buscar(seguido);
		Perfil p2 = getUsers().buscar(seguidor);
		try {
			if(p1==null || p2==null) {
				throw new PIException();
			}else { //se ambos existirem
				if(p1.isAtivo()==false || p2.isAtivo()==false) {
					throw new PDException();
				}else {//os dois est�o ativos
					if(seguidor.equals(seguido)) {
						throw new SIException();
					}else {//s�o diferentes, td ok
						p1.addSeguidor(p2);
						p2.addSeguido(p1);
						p2.getTimeline().addAll(tweets(p1.getUsuario())); //adicionando os tweets a timeline
						System.out.println("agr o @"+seguidor+ " segue o @"+seguido);
					}
				}
			}
		}catch(PIException e){
			e.printStackTrace();
		}catch(PDException e2){
			e2.printStackTrace();
		}catch(SIException e3){
			e3.printStackTrace();
		}
		
	}

	public int numeroSeguidores(String usuario) throws PIException, PDException {
		Perfil perfil = getUsers().buscar(usuario);
		try {
			if(perfil==null) {
				throw new PIException();
			}else if(perfil.isAtivo()==false) {
				throw new PDException();
			}else {
				return perfil.getSeguidores().size();
			}
		}catch(PIException e) {
			e.printStackTrace();
			return 0;
		}catch(PDException e2) {
			e2.printStackTrace();
			return 0;
		}
		
	}

	public Vector<Perfil> seguidores(String usuario) {
		Perfil perfil = getUsers().buscar(usuario);
		try {
			if(perfil==null) {
				throw new PIException();
			}else if(perfil.isAtivo()==false) {
				throw new PDException();
			}else {
				return perfil.getSeguidores();
			}
		}catch(PIException e) {
			e.printStackTrace();
			return null;
		}catch(PDException e2) {
			e2.printStackTrace();
			return null;
		}
	} //como ha uma verificacao se o usuario que vai seguir eh inativo no metodo seguir, n precisamos verificar aqui dnv, eu achO

	public Vector<Perfil> seguidos(String usuario) {
		Perfil perfil = getUsers().buscar(usuario);
		try {
			if(perfil==null) {
				throw new PIException();
			}else if(perfil.isAtivo()==false) {
				throw new PDException();
			}else {
				return perfil.getSeguidos();
			}
		}catch(PIException e) {
			e.printStackTrace();
			return null;
		}catch(PDException e2) {
			e2.printStackTrace();
			return null;
		}
	}


	public Vector<Tweet> timeline(String usuario) {
		Perfil perfil = getUsers().buscar(usuario);
		try {
			if(perfil==null) {
				throw new PIException();
			}else if(perfil.isAtivo()==false) {
				throw new PDException();
			}else {
				return perfil.getTimeline();
			}
		}catch(PIException e) {
			e.printStackTrace();
			return null;
		}catch(PDException e2) {
			e2.printStackTrace();
			return null;
		}
	}

	public IRepositorioUsuario getUsers() {
		return users;
	}

	public void setUsers(IRepositorioUsuario users) {
		this.users = users;
	}
}
