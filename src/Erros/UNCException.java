package Erros;

public class UNCException extends Exception {
	public UNCException(String usuario) {
		super("A conta @" + usuario + " nao existe!");
	}
}
