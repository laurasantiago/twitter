package Repos;

import Erros.UJCException;
import Pessoa.Perfil;

public interface IRepositorioUsuario {
	public void cadastrarUsuario(Perfil usuario) throws UJCException;
	public Perfil buscar(String usuario);
	public void atualizar(Perfil usuario);
	public void descadastrarUsuario(Perfil usuario);
}
