package Repos;

import java.util.Vector;

import Erros.MFPException;
import Erros.PDException;
import Erros.PEException;
import Erros.PIException;
import Erros.SIException;
import Erros.UJCException;
import Pessoa.*;


public interface ITwitter {
	public void criarPerfil(Perfil usuario) throws PEException, UJCException;
	public void cancelarPerfil(Perfil usuario) throws PIException, PDException;
	public void tweetar(String usuario, String mensagem) throws PIException, MFPException;
	public Vector<Tweet> timeline(String usuario);
	public Vector<Tweet> tweets(String usuario) throws PIException, PDException;
	public void seguir(String seguidor, String seguido) throws PIException, PDException, SIException;
	public int numeroSeguidores(String usuario) throws PIException, PDException;
	public Vector<Perfil> seguidores(String usuario);
	public Vector<Perfil> seguidos(String usuario);
	

}
