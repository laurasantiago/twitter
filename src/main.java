import java.util.Scanner;
import java.util.Vector;

import Erros.MFPException;
import Erros.PDException;
import Erros.PEException;
import Erros.PIException;
import Erros.SIException;
import Erros.UJCException;
import Erros.UNCException;
import Pessoa.*;
//ERRO A TRATAR: SEGUIR ALGUEM 2X
public class main {
	public static void main(String args[]) throws UJCException, PEException, PIException, PDException, MFPException, SIException {
		//teste1();
		VectorPerfil vetor = new VectorPerfil(); //repo do twitter td
		MyTwitter conta = new MyTwitter(vetor);
		Scanner read = new Scanner(System.in);
		int e = 0;
		//testeTwitter(vetor);
		while(e!=11) {
			System.out.println("bem vindo ao twitter! Selecione o que dejesa fazer:");
			System.out.println("1 para criar um perfil\n2 para cancelar um perfil\n3 para tweetar\n4 para ver uma timeline");
			System.out.println("5 para ver tweets de alguem\n6 para seguir alguem\n7 para ver o numero de seguidores\n8 para ver os seguidores\n9 para ver os seguidos");
			System.out.println("10 paraatualizar um perfil\n11 para sair");
			e = read.nextInt();
			if(e==1) {
				criar(conta);
			}
			if(e==2) {
				cancelar(conta);
			}
			if(e==3) {
				tweetar(conta);
			}
			if(e==4) {
				timeline(conta);
			}
			if(e==5) {
				tweets(conta);
			}
			if(e==6) {
				seguir(conta);
			}
			if(e==7) {
				numSeguidores(conta);
			}
			if(e==8) {
				seguidores(conta);
			}
			if(e==9) {
				seguindo(conta);
			}
			if(e==10) {
				atualizar(conta);
			}
		}
		
		
		
		
	}
	
	//CRIAR CONTA
	public static void criar(MyTwitter conta) throws PEException, UJCException {
		int e = 0;
		long num = 0;
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario:");
		String us = ler.next();
		System.out.println("insira 1 para pessoa fisica ou 2 para juridica");
		e = ler.nextInt();
		if(e==1) {
			Perfil p = new PessoaFisica(us);
			System.out.println("insira o CPF:");
			num = ler.nextLong();
			((PessoaFisica) p).setCpf(num);
			conta.criarPerfil(p);
		}else if(e==2) {
			Perfil p = new PessoaJuridica(us);
			System.out.println("insira o CNPJ:");
			num = ler.nextLong();
			((PessoaJuridica) p).setCnpj(num);
			conta.criarPerfil(p);
		}
		
	}
	//CANCELAR CONTA
	public static void cancelar(MyTwitter conta) throws PIException, PDException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario:");
		String us = ler.next();
		Perfil p = conta.getUsers().buscar(us);
		conta.cancelarPerfil(p);
	}
	//TWEETAR
	public static void tweetar(MyTwitter conta) throws PIException, MFPException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario:");
		String us = ler.next();
		System.out.println("insira o tweet! (entre 1 e 140 caracteres)");
		ler.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		String tt = ler.nextLine();
		conta.tweetar(us, tt);
	}
	//VER UMA TIMELINE
	public static void timeline(MyTwitter conta) {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario:");
		String us = ler.next();
		try {
			Vector<Tweet> tl = conta.timeline(us);
			int i = 0;
			Perfil p;
			String u;
			while(i<tl.size()) {
				u = tl.get(i).getUsuario(); //usu�rio do tweet atual
				p = conta.getUsers().buscar(u); //o perfil desse usuario
				if(p.isAtivo()==true) {
					System.out.println(tl.get(i).getMensagem());
				}
				else {
					System.out.println("--usuario inativo--");
				}
				i = i + 1;
				
		}
	}catch(java.lang.NullPointerException e1) {
		System.out.println("nao foi possivel completar a acao");
		}
	}
	
	//SEGUIR ALGUEM
	public static void seguir(MyTwitter conta) throws PIException, PDException, SIException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario que vai seguir a pessoa:");
		String p1 = ler.next();
		System.out.println("insira o usuario que vai ser seguido:");
		String p2 = ler.next();
		conta.seguir(p1, p2);
	}
	//VER TWEETS DE ALGUEM
	public static void tweets(MyTwitter conta) throws PIException, PDException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario q deseja ver os tweets:");
		String p = ler.next();
		try {
			Vector<Tweet> t = conta.tweets(p);
			int i = 0;
			while(i<t.size()) {
				System.out.println(t.get(i).getMensagem());
				i = i + 1;
			}
		}catch(java.lang.NullPointerException e) {
			System.out.println("n foi possivel completar a acao!");
		}
	}
	//QUANT DE SEGUIDORES
	public static void numSeguidores(MyTwitter conta) throws PIException, PDException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario q deseja ver a os seguidores:");
		String p = ler.next();
		try {
			Vector<Perfil> seg = conta.seguidores(p);
			int i = 0;
			int res = seg.size();
			while(i<seg.size()) {
				if(seg.get(i).isAtivo()==false) {
					res = res - 1;
				}
				i = i + 1;
			}
			System.out.println(res);
		}catch(java.lang.NullPointerException e) {
			System.out.println("n foi possivel completar a acao!");
		}
	}
	//SEGUIDORES
	public static void seguidores(MyTwitter conta) {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario q deseja ver a os seguidores:");
		String p = ler.next();
		try {
			Vector<Perfil> seg = conta.seguidores(p);
			int i = 0;
			while(i<seg.size()) {
				if(seg.get(i).isAtivo()==true) {
					System.out.println(seg.get(i).getUsuario());
				}
				i = i+1;
			}
		}catch(java.lang.NullPointerException e) {
			System.out.println("n foi possivel completar a acao!");
		}
	}
	
	//SEGUINDO
	public static void seguindo(MyTwitter conta) {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario q deseja ver a quem segue:");
		String p = ler.next();
		try {
			Vector<Perfil> seg = conta.seguidos(p);
			int i = 0;
			while(i<seg.size()) {
				if(seg.get(i).isAtivo()==true) {
					System.out.println(seg.get(i).getUsuario());
				}
				i = i+1;
			}
		}catch(java.lang.NullPointerException e) {
			System.out.println("n foi possivel completar a acao!");
		}
	}
	
	//ATUALIZAR
	public static void atualizar(MyTwitter conta) {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira o usuario q deseja atualizar:");
		String p = ler.next();
		try {
			Perfil us = conta.getUsers().buscar(p);
			conta.getUsers().atualizar(us);
		}catch(java.lang.NullPointerException e) {
			System.out.println("n foi possivel completar a acao!");
		}
	}
	
	public static void testeTwitter(VectorPerfil vetor) throws PEException, UJCException, PIException, PDException, MFPException, SIException  {
		MyTwitter conta = new MyTwitter(vetor);
		popular(conta, vetor);
	}
	public static void popular(MyTwitter conta, VectorPerfil vetor) throws PEException, UJCException, PIException, PDException, MFPException, SIException {
		Scanner ler = new Scanner(System.in);
		
		Perfil p1 = new PessoaFisica("laaaaaaa45");
		((PessoaFisica) p1).setCpf(111);
		Perfil p2 = new PessoaFisica("batatafrita7");
		conta.criarPerfil(p1);
		conta.criarPerfil(p2);
		Perfil p3 = new PessoaJuridica("gtijr");
		conta.criarPerfil(p3);
		Perfil p4 = new PessoaFisica("gtijr");
		conta.criarPerfil(p4);
		Perfil p5 = new PessoaFisica("lauraaa");
		conta.criarPerfil(p5);
//		vetor.atualizar(p2);
//		System.out.println("insira o perfil p cancelar");
//		String nome = ler.next();
//		Perfil usuario = vetor.buscar(nome);
//		conta.cancelarPerfil(usuario);
		
//		System.out.println("insira o usuario que vai tuitar");
//		String user = ler.next();
//		System.out.println("insira o tweet");
//		//SEMPRE DEIXAR ESSES 2 JUNTOS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		ler.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//		String tweet = ler.nextLine();
//		//FIM DO COMENTARIO MTMTMTM T IMPORTANT
//		conta.tweetar(user, tweet);
//		
//		
//		System.out.println("insira o usuario que vai tuitar");
//		String user3 = ler.next();
//		System.out.println("insira o tweet");
//		//SEMPRE DEIXAR ESSES 2 JUNTOS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		ler.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//		String tweet2 = ler.nextLine();
//		//FIM DO COMENTARIO MTMTMTM T IMPORTANT
//		conta.tweetar(user3, tweet2);
//		
//		//TWEETS DA PESSOA
//		System.out.println("de qual user quer ver os tweets?");
//		String user2 = ler.next();
//		int i = 0;
//		Vector<Tweet> tweets = conta.tweets(user2);
//		while(i<tweets.size()) {
//			if(tweets.get(i).getUsuario().equals(user2)) {
//				System.out.println(tweets.get(i).getMensagem());
//				i = i + 1;
//			}
//			
//		}
		
		System.out.println("insira o usuario quem vai ser seguido");
		String userflg = ler.next();
		System.out.println("insira o usuario quem vai seguir");
		String userflr = ler.next();
		conta.seguir(userflr, userflg);
		System.out.println("deseja ver a quant de seguidores de qual usuario?");
		String brad = ler.next();
		System.out.println(conta.numeroSeguidores(brad));
		
		
	}
	public static void teste2() throws UJCException {
		VectorPerfil vetor = new VectorPerfil();
		criar(vetor);
		criar(vetor);
		atualizar(vetor);
		
	}
	public static void criar(VectorPerfil vetor) throws UJCException {
		System.out.println("crie um user pessoa fisica");
		Scanner ler = new Scanner(System.in);
		System.out.println("insira um usuario");
		String nome = ler.next();
		System.out.println("insira um cpf");
		long cpf = ler.nextLong();
		Perfil p2 = new PessoaFisica(nome);
		((PessoaFisica) p2).setCpf(cpf);
		vetor.cadastrarUsuario(p2);
		
		
		
	}
	
	public static void atualizar(VectorPerfil vetor) throws UJCException {
		Scanner ler = new Scanner(System.in);
		System.out.println("insira um usuario");
		String nome = ler.next();
		Perfil p = vetor.buscar(nome);
		try {
			if(p==null) {
				throw new UNCException(nome); //se o usuario n existr
			}
			vetor.atualizar(p);
		}catch(UNCException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void teste1() throws UJCException {
		VectorPerfil x = new VectorPerfil();
		Perfil p1 = new PessoaFisica("laaaaaaa45");
		((PessoaFisica) p1).setCpf(111);
		Perfil p2 = new PessoaFisica("laaaaaaa45");
		((PessoaFisica) p2).setCpf(111);
		x.cadastrarUsuario(p1);
		x.cadastrarUsuario(p2);
		Perfil p3 = new PessoaFisica("batatafrita7");
		x.cadastrarUsuario(p3);
		x.buscar("batatafrita7");
		x.buscar("batatafrita");
		x.atualizar(p2);
		x.atualizar(p2);
		
	}
}
/*
1
oi
1
42185
1
aa
2
4534534
3
oi
oi, eu sou oi
3
aa
oi, im aa
6
oi
aa
4
oi
*/